package cn.zb.interceptor;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;


import cn.zb.make.Message;

public class SessionVerify implements HandlerInterceptor {
	@Autowired
	private Message message;
	@Autowired
	private ObjectMapper	objectMapper ;
	@Override
	public void afterCompletion(HttpServletRequest arg0,
			HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub					
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1,
			Object arg2, ModelAndView arg3) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
			Object object) throws Exception {
		// TODO Auto-generated method stub
		if(request.getSession().getAttribute("verifyCode")==null){							
			String jsonString = objectMapper.writeValueAsString( message.getMessage(200, "验证码不存在！"));          
			response.setContentType("json");
			response.getWriter().print(jsonString);
			return false;
		}
		else
			return true;		
	}

}
