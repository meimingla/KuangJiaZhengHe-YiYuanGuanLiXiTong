package cn.zb.make;

import org.springframework.stereotype.Component;

@Component
public class Message {
   /*
    * status 100 成功
    * status 200 失败
    */
	private Integer status;
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	private String message;
	public Message getMessage(int status,String message){
		this.setMessage(message);
		this.setStatus(status);
		return this;
	}
}
