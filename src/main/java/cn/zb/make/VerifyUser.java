package cn.zb.make;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cn.zb.pojo.User;
import cn.zb.service.GoodService;

@Component
public class VerifyUser {
	@Resource
	private Message message;
	@Autowired
	private GoodService goodService;
	public Object verify(String name,User user){
		if(user!=null){
			if(user.getName().equals(name)){
				
				return true;
			}
			else{	  
				return message.getMessage(200, "权限不足");
			}
		}
		else
			return message.getMessage(200, "请登录！");
	}
	public Object verify(User user,int tId,Boolean status){
		if(user!=null){
			if(goodService.isGood(user.getId(),tId)){
			 return goodService.insert(user,status,tId);				
			}
			else{	  
				return message.getMessage(200, "您已投票！");
			}
		}
		else
			return message.getMessage(200, "请登录！");
	}

}
