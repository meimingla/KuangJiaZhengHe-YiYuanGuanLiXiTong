package cn.zb.make;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.springframework.stereotype.Component;
@Component
public class GetTime {
	public String timeChang(Date insertTime)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");  
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));  
        String res =  sdf.format(insertTime);
        return res;  
	}
	public String createTime()
	{
		SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
		String time=formater.format(new Date());
		return time;
	}
}
