package cn.zb.make;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import cn.zb.pojo.*;
@Component
public class SendEmail {
		@Autowired
		private JavaMailSenderImpl mailSender;
		public boolean send(User user) {	
			Boolean b=false;
			MimeMessage m = mailSender.createMimeMessage();			
			try {
					MimeMessageHelper	helper = new MimeMessageHelper(m, true, "UTF-8");
					// 设置收件人
			         helper.setTo(user.getEmail());
		//////	         // 设置抄送
		////////	       helper.setCc("");
		 	             // 设置发件人
			         helper.setFrom(mailSender.getUsername());
		//////	         // 设置暗送
//			         helper.setBcc("");
			             // 设置主题
			         helper.setSubject("密码找回！");
			             // 设置 HTML 内容
			              helper.setText("您的密码是"+user.getPassword()+",请妥善保管！");        
		////	         // 进行邮件发送和邮件持久类的状态设定z
			         mailSender.send(m);
			         b=true;
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	         
	         return b;
		}
}
