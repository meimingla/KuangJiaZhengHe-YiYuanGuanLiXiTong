package cn.zb.make;


import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import cn.zb.config.FilePath;

@Component
public class CreateFilePath {
	@Resource
	private FilePath filePath;
	public String getUserImagePath(CommonsMultipartFile multipartFile){
	 String suffix=	multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf("."), multipartFile.getOriginalFilename().length());
		return filePath.getUserHeadImagePath()+suffix;
	}
	public String getworkerImagePath(CommonsMultipartFile multipartFile){
		 String suffix=	multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf("."), multipartFile.getOriginalFilename().length());
			return filePath.getWorkerHeadImagePath()+suffix;
		}
	public String getFilePath(CommonsMultipartFile multipartFile){
		 String suffix=	multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf("."), multipartFile.getOriginalFilename().length());
			return filePath.getFilePath()+suffix;
		}
	
}
