package cn.zb.make;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;
@Component
public class Verify {
	//验证验证码是否正确		
	public Boolean judge (HttpSession session,String verifyCode){
		if(session.getAttribute("verifyCode").equals(verifyCode))
			return true;
		else
			return false;
	}
}
