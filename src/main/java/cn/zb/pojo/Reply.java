package cn.zb.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table
@JsonIgnoreProperties(value={"topic","reply"})  
public class Reply implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2546485252528901243L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="tId",nullable=false)
	private Topic topic;
	@Lob
	private String content; 
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="uId",nullable=false)
	private User user;
	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="fId")
	private Set<Reply> replyList;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fId")
	private Reply reply;
	@Temporal(TemporalType.DATE)
	private Date time;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Topic getTopic() {
		return topic;
	}
	public void setTopic(Topic topic) {
		this.topic = topic;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Set<Reply> getReplyList() {
		return replyList;
	}
	public void setReplyList(Set<Reply> replyList) {
		this.replyList = replyList;
	}
	public Reply getReply() {
		return reply;
	}
	public void setReply(Reply reply) {
		this.reply = reply;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
}
