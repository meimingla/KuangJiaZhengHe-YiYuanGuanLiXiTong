package cn.zb.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Parent;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name="user",uniqueConstraints={@UniqueConstraint(columnNames={"email"})})
@JsonIgnoreProperties(value={"topics","userFiles","replys"}) 
public class User implements Serializable {
	public User(){
		
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1081140154534312198L;
	@Id
	@Column(name="id",length=4)
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
	private int id;
	@Column(name="name",length=8)
	private String name; 
	@Column(name="password",length=16)
	private String password;
	@Column(name="email",length=20,nullable=false)
	@Email
	@NotEmpty
	private String email;
	@Column(name="QQ",length=12)
	@NotEmpty
	private String QQ;
	@Column(name="tel",length=11)
	private String tel;
	@Column(name="intoTime")
	@Temporal(TemporalType.DATE)
	private Date intoTime ;
	@Column(name="headImage")
	@Lob
	private String headImage;
	@OneToMany(cascade={CascadeType.REMOVE},fetch=FetchType.LAZY)
	@JoinColumn(name="user_id")
	private List<UserFile> userFiles;
	@OneToMany(cascade={CascadeType.REMOVE},fetch=FetchType.LAZY)	
	@JoinColumn(name="UId")
	private List<Topic> topics;
	@OneToMany(cascade={CascadeType.REMOVE},fetch=FetchType.LAZY)	
	@JoinColumn(name="uId")
	private List<Reply> replys;
	@OneToMany(cascade={CascadeType.REMOVE},fetch=FetchType.LAZY)	
	@JoinColumn(name="uId")
	private List<Good> goods;
	public List<Reply> getReplys() {
		return replys;
	}
	public void setReplys(List<Reply> replys) {
		this.replys = replys;
	}
	public List<Topic> getTopics() {
		return topics;
	}
	public void setTopics(List<Topic> topics) {
		this.topics = topics;
	}
	public List<UserFile> getUserFiles() {
		return userFiles;
	}
	public void setUserFiles(List<UserFile> userFiles) {
		this.userFiles = userFiles;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getIntoTime() {
		return intoTime;
	}
	public void setIntoTime(Date intoTime) {
		this.intoTime = intoTime;
	}
	public String getHeadImage() {
		return headImage;
	}
	public void setHeadImage(String headImage) {
		this.headImage = headImage;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getQQ() {
		return QQ;
	}
	public void setQQ(String qQ) {
		QQ = qQ;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
}
