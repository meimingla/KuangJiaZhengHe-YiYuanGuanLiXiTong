package cn.zb.pojo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cascade;
@Entity
@Table(name="user_file",uniqueConstraints={@UniqueConstraint(columnNames={"file_name"})})
public class UserFile implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1289253080131428392L;
	@Id
	@Column(length=4)
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
	private int id;
	@ManyToOne(fetch=FetchType.EAGER)	
	@JoinColumn(name="user_id")
	private User user;
	@Column(name="upload_date")
	@Temporal(TemporalType.DATE)
	private Date uploadDate;
	@Column(length=10,name="file_name",nullable=false)	
	private String fileName;
	@Column(length=10,name="file_size")
	private String fileSize;
	@Column(name="file_path")
	private String filePath;
	@Column(length=10,name="download_count")
	private int downloadCount;
	public void setUser(User user) {
		this.user = user;
	}
	public User getUser() {
		return user;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getUploadDate() {
		return uploadDate;
	}
	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileSize() {
		return fileSize;
	}
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	public int getDownloadCount() {
		return downloadCount;
	}
	public void setDownloadCount(int downloadCount) {
		this.downloadCount = downloadCount;
	}
	

}
