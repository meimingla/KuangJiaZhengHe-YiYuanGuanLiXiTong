package cn.zb.pojo;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@Entity
@Table(name="section")
//将不会把list作为元素做转换
@JsonIgnoreProperties(value={"section"})   
public class Section implements Serializable {
		/**
	 * 
	 */
	private static final long serialVersionUID = -8903174750865394314L;
		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column
		private Integer id;
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getText() {
			return text;
		}
		public void setText(String text) {
			this.text = text;
		}
		
		public Section getSection() {
			return section;
		}
		public void setSection(Section section) {
			this.section = section;
		}
		public Set<Section> getChildren() {
			return children;
		}
		public void setChildren(Set<Section> children) {
			this.children = children;
		}
		@Column(length=10,nullable=false,unique=true,name="name")
		private String text;
		@OneToMany(cascade=CascadeType.REMOVE,fetch=FetchType.EAGER)
		@JoinColumn(name="FId")
		private Set<Section> children;
		@ManyToOne(fetch=FetchType.LAZY)
		@JoinColumn(name="FId")
		private Section section;
		private String state;
		public String getState() {
			return state;
		}		
		private String iconCls;
		public String getIconCls() {
			return iconCls;
		}
		public void setIconCls(String iconCls) {
			this.iconCls = iconCls;
		}
		public void setState(String state) {
			this.state = state;
		}
		@OneToMany(cascade=CascadeType.REMOVE,fetch=FetchType.EAGER)
		@JoinColumn(name="SId")
		private Set<Topic> topics;
		public Set<Topic> getTopics() {
			return topics;
		}
		public void setTopics(Set<Topic> topics) {
			this.topics = topics;
		}
		
}
