package cn.zb.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@Entity
@Table(name="topic")
@JsonIgnoreProperties(value={"section"})   
public class Topic implements Serializable {
		/**
	 * 
	 */
	private static final long serialVersionUID = 5898297934085187819L;
		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		private int id;
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getContent() {
			return content;
		}
		public void setContent(String content) {
			this.content = content;
		}
		@JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
		public Date getPublish() {
			return publish;
		}
		public void setPublish(Date publish) {
			this.publish = publish;
		}
		public Section getSection() {
			return section;
		}
		public void setSection(Section section) {
			this.section = section;
		}
		public User getUser() {
			return user;
		}
		public void setUser(User user) {
			this.user = user;
		}
		private String title;
		@Lob
		private String content;
		@Temporal(TemporalType.DATE)
		private Date publish;
		@ManyToOne(fetch=FetchType.EAGER)
		@JoinColumn(name="SId",nullable=false)
		private Section section;
		@ManyToOne(fetch=FetchType.EAGER)
		@JoinColumn(name="UId",nullable=false)
		private  User user;
		@OneToMany(fetch=FetchType.EAGER)
		@JoinColumn(name="tId")	
		private List<Reply> replyList;
		@OneToMany(fetch=FetchType.EAGER)
		@JoinColumn(name="tId")	
		private Set<Good> goods;		
		public Set<Good> getGoods() {
			return goods;
		}
		public void setGoods(Set<Good> goods) {
			this.goods = goods;
		}
		public List<Reply> getReplyList() {
			return replyList;
		}
		public void setReplyList(List<Reply> replyList) {
			this.replyList = replyList;
		}
}
