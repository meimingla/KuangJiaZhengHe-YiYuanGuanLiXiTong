package cn.zb.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import cn.zb.pojo.Good;

@Repository
public class GoodDao {
	@Autowired
		private HibernateTemplate template;
	public List<Good> select (int uId,int tId)
	{
	      return	(List<Good>) template.find("from Good where uId=? and tId=?", new Object[] {uId,tId});
	}
	public boolean insert(Good good){
		template.save(good);
		return true;
	}
}
