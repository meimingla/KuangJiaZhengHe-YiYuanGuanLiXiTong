package cn.zb.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import cn.zb.pojo.Reply;

@Repository
public class ReplyDao {
	@Autowired
	private HibernateTemplate template;
	public void insert(Reply reply){
		template.save(reply);
	}
	//查询特定评论
	public Reply theSelect(int id){
		return template.load(Reply.class, id);
	}

}
