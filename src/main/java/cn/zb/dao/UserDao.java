package cn.zb.dao;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import cn.zb.pojo.User;
@Repository
public class UserDao {
	@Resource
	HibernateTemplate template;

	public boolean insert(User user){
		template.save(user);
		return true;		
	}
	public User getEmail(String email){	
		System.out.println(email);
		List<User> list=(List<User>) template.find("from User where email=?",email);
		System.out.println(list.size());
		if(list.size()>0)		
		 return list.get(0);	
		return null;
	}
	public User getAccount(User user){		
		List<User> list=(List<User>) template.find("from User where email=? and password=?",new Object[]{user.getEmail(),user.getPassword()});
		if(list.size()>0)
			return list.get(0);
		return null;	
	}

}
