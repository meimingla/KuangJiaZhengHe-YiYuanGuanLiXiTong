package cn.zb.dao;

import java.util.List;

import javax.annotation.Resource;





import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import cn.zb.pojo.UserFile;
@Repository
public class UserFileDao {
	@Resource
	private HibernateTemplate template;	
	public List<UserFile> select(){	 
	  return  (List<UserFile>) template.find("from UserFile", null);
	}

}
