package cn.zb.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.zb.dao.ReplyDao;
import cn.zb.dao.TopicDao;
import cn.zb.pojo.Reply;
import cn.zb.pojo.Topic;
import cn.zb.pojo.User;
@Service
public class ReplyService {
	@Autowired
	private ReplyDao replyDao;
	@Autowired
	private TopicDao topicDao;
	//添加子评论
	@Transactional
	public Boolean insertChildren(User user,String content,int rId){
		Reply fr=replyDao.theSelect(rId);
		Reply reply=new Reply();
		reply.setUser(user);
		reply.setTime(new Date());
		reply.setTopic(fr.getTopic());
		reply.setReply(fr);
		reply.setContent(content);
		replyDao.insert(reply);
		return true;
	}
	//添加评论
		@Transactional
		public Boolean insert(User user,String content,int tId){
			Topic topic=topicDao.theSelect(tId);
			Reply reply=new Reply();
			reply.setUser(user);
			reply.setTime(new Date());
			reply.setTopic(topic);
			reply.setReply(null);
			reply.setContent(content);
			replyDao.insert(reply);
			return true;
		}

}
