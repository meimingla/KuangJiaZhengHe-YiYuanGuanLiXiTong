package cn.zb.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.zb.dao.UserDao;
import cn.zb.make.Message;
import cn.zb.make.SendEmail;
import cn.zb.pojo.User;
@Service
public class UserService {
	@Resource
	private UserDao userDao;
	@Resource
	private Message message;
	@Resource
	private SendEmail sendEmail;
	@Transactional
	public Boolean userRegister(User user){
	    return	userDao.insert(user);
	}
	@Transactional
	public User userLogin(User user){
	    return	userDao.getAccount(user);
	}
	@Transactional
	public Message getUser(String email) {
		User user=userDao.getEmail(email);
		if(user!=null)
		{		
			if(sendEmail.send(user)){			
			message.setStatus(100);
			message.setMessage("亲爱的"+user.getName()+",密码已下发到您的邮箱，请注意查收");}
			else{
				message.setStatus(200);
				message.setMessage("邮件发送失败");
			}
		}
		else{
			message.setStatus(200);
			message.setMessage("该用户没有注册");
		}		
		return message; 
	}
}
