package cn.zb.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.zb.dao.UserDao;
import cn.zb.pojo.User;

@Service
public class VerifyService {
	@Resource
	private UserDao userDao;
	@Transactional
	public boolean emailVerify(String email){	
		if(userDao.getEmail(email)!=null)
		return false;
		else
			return true;
	}

}
