package cn.zb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.zb.dao.GoodDao;
import cn.zb.pojo.Good;
import cn.zb.pojo.User;
@Service
public class GoodService {
	@Autowired
	private GoodDao goodDao;
	@Autowired
	private ForumService forumService;
	@Transactional
	public Boolean isGood(int uId,int tId){
		if(goodDao.select(uId, tId).size()>0)
			return false;
		else
			return true;
	}
	//添加点赞信息
	@Transactional
	public Boolean insert(User user,Boolean status,int tId ){
		Good good=new Good();
		good.setStatus(status);
		good.setUser(user);
		good.setTopic(forumService.getTopic(tId));
		return goodDao.insert(good);
	}

}
