package cn.zb.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import cn.zb.dao.UserFileDao;
import cn.zb.make.CreateFilePath;
import cn.zb.pojo.UserFile;
@Service
public class FileService {
	@Resource
	private UserFileDao fileDao;
	@Resource
	private CreateFilePath filePath;
	public CreateFilePath getFilePath() {
		return filePath;
	}
	public void setFilePath(CreateFilePath filePath) {
		this.filePath = filePath;
	}
	//保存单个文件
	public boolean savaFile(HttpServletRequest request,CommonsMultipartFile multipartFile) {
		if(multipartFile!=null){
		 String path=request.getSession().getServletContext().getRealPath("")+filePath.getUserImagePath(multipartFile);
		 File file=new File(path.substring(0,path.lastIndexOf("\\")));
		    if(!file.exists()){
		    	file.mkdirs();
		    }
		    try {
				multipartFile.transferTo(new File(path));
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();return false;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();return false;
			}
		return true;}
		else
			return false;
		
	}
	//获取文件表数据
	public List<UserFile> select(){
		return fileDao.select();
	}
	//多文件上传

}
