package cn.zb.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.zb.dao.SectionDao;
import cn.zb.dao.TopicDao;
import cn.zb.pojo.Section;
import cn.zb.pojo.Topic;
@Service
public class ForumService {
	@Resource
	private SectionDao sectionDao;	
	@Resource
	private TopicDao topicDao;
	@Transactional
	public List<Section> getTree(){
		return (List<Section>) sectionDao.select();
	}
	//查询某区域帖子
	@Transactional
	public List<Topic> getTopics(int id){
		return topicDao.select(id);
	}
	//分页查询某区域帖子
	@Transactional
	public List<Topic> pageSectionTopics(int id,int page,int limit){
		return topicDao.select( id,page,limit);
	}
	//分页查询所有帖子
	@Transactional
	public List<Topic> pageTopics(int page,int limit){
		return  topicDao.select(page,limit);
	}
	//查询所有帖子
	@Transactional
	public List<Topic> getTopics(){
		return topicDao.select();
	}
	//查询特定帖子
	@Transactional
	public Topic getTopic(int id){
		return topicDao.theSelect(id);
	}
	@Transactional
	public boolean insertTopic(Topic topic){
		return topicDao.insert(topic);
	}
	@Transactional
	public boolean delectTopic(int id){		
		return topicDao.delect(id);
	}
	@Transactional
	public boolean updateTopic(Topic topic){
		return topicDao.update(topic);
	}
	@Transactional
	public List<Topic> likeTopic(String title){		
		return topicDao.select(title);
	}
	@Transactional
	public List<Topic> likeTopic(String title,int page,int limit){		
		return topicDao.select(title ,page,limit);
	}
}
