package cn.zb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
@Controller
@RequestMapping(value="/Main")
public class MainController {
	@RequestMapping(value="/news")
	public String news()
	{		
		return "user/news";
	}
	@RequestMapping(value="/forum")
	public String froum()
	{		
		
		return "forum/forum";
	}
	@RequestMapping(value="/onlineChat")
	public String onlineChat()
	{		
		return "user/onlineChat";
	}
	@RequestMapping(value="/onlineOrder")
	public String onlineOrder()
	{		
		return "user/onlineOrder";
	}
	@RequestMapping(value="/fileManage")
	public String fileManage()
	{		
		return "user/fileManage";
	}
	@RequestMapping("/register")
	public String register()
	{		
		return "user/register";
	}
	@RequestMapping("/main")
	public String main()
	{		
		return "main";
	}
	@RequestMapping("/forget")
	public String getPassword()
	{		
		return "user/getpassword";
	}
	@RequestMapping("/login")
	public String login()
	{		
		return "user/login";
	}
}
