package cn.zb.controller;



import java.util.Date;
import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import cn.zb.make.GetTime;
import cn.zb.make.Message;
import cn.zb.make.Verify;
import cn.zb.pojo.User;
import cn.zb.service.FileService;
import cn.zb.service.UserService;
import cn.zb.service.VerifyService;

@Controller
@RequestMapping("/outLogin")
public class OutLoginController {
	@Resource
	private UserService userService;
	@Resource
	private VerifyService verifyService;
	@Resource
	private Message message;
	@Resource
	private GetTime getTime;
	@Resource
	private Verify verify;
	@Resource
	private FileService fileService;
	/*
	 * 用户注册
	 */
	@RequestMapping("/register")
	public@ResponseBody Message register(@SessionAttribute(name="verifyCode") String verifyCode, HttpServletRequest request,String verify,@RequestParam(value="headImg",required=false)CommonsMultipartFile multipartFile,User user) {				
		if(verifyCode.equals(verify)){
			if(fileService.savaFile(request, multipartFile)){
				user.setHeadImage(fileService.getFilePath().getUserImagePath(multipartFile));
				user.setIntoTime(new Date());
				 if(userService.userRegister(user))
					 return message.getMessage(100, "注册成功");
				 else
					 return message.getMessage(200, "注册失败!");				 
				}
			else
				return message.getMessage(200, "头像保存失败!");				    			    
		}	
		else
				return message.getMessage(200, "验证码输入错误");		
	}
	/*
	 * 邮箱验证
	 */
	@RequestMapping("/verifyEmail")
	public @ResponseBody String verityEmail(String email) {
		
		return String.valueOf(verifyService.emailVerify(email));
	}
	/*
	 * 用户登录
	 */
	@RequestMapping("/login")
	public @ResponseBody Message login(User user,HttpSession session) {	
		User entity=userService.userLogin(user);
		 if(entity!=null)
		    {
			 	
		    	message.setStatus(100);
				message.setMessage("登陆成功");
				session.setAttribute("user", entity);
		    }
		    else{
		    	message.setStatus(200);
				message.setMessage("邮箱或密码错误");
		    }			
			return message;
		}	
	//密码找回
	@RequestMapping("/getpassword")
	public @ResponseBody Message getPassword(@Valid User user,BindingResult result){		
		if(result.getAllErrors().size()>0){
			return message.getMessage(200,result.getAllErrors().get(1).getDefaultMessage());
		}
		
		else	return userService.getUser(user.getEmail());			
	}
	//用户退出
	@RequestMapping("exit")
	public String exit(HttpSession session){
		session.removeAttribute("user");	
		return "main";
	}
	@RequestMapping("/test")
	public void s() throws MessagingException {
	
		
	}
	
}
