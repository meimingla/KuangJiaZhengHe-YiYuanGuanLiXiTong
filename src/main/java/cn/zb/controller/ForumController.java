package cn.zb.controller;


import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;

import cn.zb.make.Message;
import cn.zb.make.Verify;
import cn.zb.make.VerifyUser;
import cn.zb.pojo.*;
import cn.zb.service.ForumService;
import cn.zb.service.ReplyService;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;
@Controller
@RequestMapping(value="/Forum")
public class ForumController {
	@Resource
	private ForumService forumService;
	@Resource
	private ReplyService replyService;
	@Resource
	private Verify verify;
	@Resource
	private Message message;
	@Resource
	private VerifyUser verifyUser;
	//模糊查询相关标题的帖子
	@RequestMapping("/likeTopic")
	public @ResponseBody List<Topic> likeTopic(String title){
		return forumService.likeTopic(title);
	}
	
	@ResponseBody
	@RequestMapping("/section")
	public  List<Section>  getTreeSection(){		
		
		return forumService.getTree();
	}
	//获得某区域的帖子
	@RequestMapping("/sectionTopic/{id}")
	public @ResponseBody List<Topic> getTopics( @PathVariable int id){		
		return forumService.getTopics(id);
	}
	//分页获得搜索的帖子
	@RequestMapping("/pageLikeTopic")
	public @ResponseBody List<Topic> pagelikeTopic( @RequestParam("title") String title,@RequestParam("page") int page,@RequestParam("limit") int limit){		
		return forumService.likeTopic(title, page, limit);
	}
	//分页获得某区域的帖子
	@RequestMapping("/pageSectionTopic/{id}")
	public @ResponseBody List<Topic> pageSectionTopics( @PathVariable int id,@RequestParam("page") int page,@RequestParam("limit") int limit){		
		return forumService.pageSectionTopics(id, page, limit);
	}
	//分页查询所有帖子
	@RequestMapping("/pageTopics")
	public @ResponseBody List<Topic> pageTopics(@RequestParam("page") int page,@RequestParam("limit") int limit ){		
		return forumService.pageTopics(page,limit);
	}
	@RequestMapping("/topic")
	public @ResponseBody List<Topic> Topics( ){		
		return forumService.getTopics();
	}
	//获取某一特定帖子
	@RequestMapping("/topic/{id}")
	public ModelAndView Topic( @PathVariable int id,ModelAndView modelAndView ){	
		modelAndView.setViewName("forum/topiccontent");
		modelAndView.addObject(forumService.getTopic(id));
		return modelAndView;
	}
	@RequestMapping("/publish")
	public String toPublish( ){		
		return "forum/sendtopic";
	}
	//发表帖子
	@RequestMapping("/publishTopic")
	public @ResponseBody Message publishTopic(@SessionAttribute(required=false) User user,@SessionAttribute String verifyCode ,Section section,String verify,Topic topic,int sId){
		if(user==null)
			return message.getMessage(200, "请登录");
		section.setId(sId);
		topic.setSection(section);
		topic.setPublish(new Date());
		topic.setUser(user);
		if(verifyCode.equals(verify)){
			if(forumService.insertTopic(topic)){
				return message.getMessage(100, "发表成功");
			}
			else{
				return message.getMessage(100, "发表失败");				
			}	
			}
		else
			return message.getMessage(200, "验证码输入错误");
	}
	//删除帖子
	@RequestMapping("/delect")
	public @ResponseBody Message delect(int id,String name,@SessionAttribute(required=false) User user){		
		if(verifyUser.verify(name, user).equals(true)){
			
				forumService.delectTopic(id);
				return message.getMessage(100, "删除成功");
		}
		else
			return (Message) verifyUser.verify(name, user);
	}
	//修改帖子
	@RequestMapping("/update")
	public @ResponseBody Message update(int id,String name,@SessionAttribute(required=false) User user){
		if(verifyUser.verify(name, user).equals(true)){			
			return message.getMessage(100, "Forum/updateTopic/");
	}
	else
		return (Message) verifyUser.verify(name, user);		
	}
	@RequestMapping("/updateTopic/{id}")
	public ModelAndView updateTopic(@PathVariable("id") int id,ModelAndView modelAndView ){		
		modelAndView.setViewName("forum/updatetopic");
		modelAndView.addObject(forumService.getTopic(id));
		return modelAndView;
	}
	//修改帖子
	@RequestMapping("/updateTopic")
	public @ResponseBody Message updateTopic(@SessionAttribute String verifyCode ,String verify,Topic topic){
		if(verifyCode.equals(verify)){
			if(forumService.updateTopic(topic)){
				return message.getMessage(100, "编辑成功");
			}
			else{
				return message.getMessage(100, "编辑失败");				
			}	
			}
		else
			return message.getMessage(200, "验证码输入错误");
	}
	//帖子点赞
	@RequestMapping("like")
	public @ResponseBody Message like(@SessionAttribute(required=false) User user,int tId,boolean status){
		if(verifyUser.verify(user,tId,status).equals(true)){
			return message.getMessage(100, "操作成功");
		}
		else 
		return(Message) verifyUser.verify(user,tId,status) ;
	}
	//发表子评论
	@RequestMapping("replyChildren")
	public @ResponseBody Message  replyChildren(@SessionAttribute(required=false) User user,String content,int rId ){
		if(user!=null){
			 if( replyService.insertChildren(user, content, rId))
				 return message.getMessage(100, "回复成功");
			 else
				 return message.getMessage(200, "回复失败！");
		}
		else
			return message.getMessage(200, "请登录！");
	}
	//发表评论
	@RequestMapping("reply")
	public @ResponseBody Message  reply(@SessionAttribute(required=false) User user,String content,int tId ){
		if(user!=null){
			 if( replyService.insert(user, content, tId))
				 return message.getMessage(100, "回复成功");
			 else
				 return message.getMessage(200, "回复失败！");
		}
		else
			return message.getMessage(200, "请登录！");
	}

}
