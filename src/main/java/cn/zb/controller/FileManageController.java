package cn.zb.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import cn.zb.service.FileService;
@Controller
@RequestMapping("/file")
public class FileManageController {
	@Resource
	private FileService fileService;
	@RequestMapping("/upload")
	public void  upload(HttpServletRequest request,@RequestParam("file")CommonsMultipartFile multipartFile){
		fileService.savaFile(request, multipartFile);
				
	}
	

}
