package cn.zb.controller;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.google.code.kaptcha.Producer;

import cn.zb.entity.VerifyCode;
import cn.zb.make.VerifyImage;

@Controller
@RequestMapping("/HelpTool")
public class HelpToolController {
	
	@Resource
	private Producer producer;
	//输出验证码
	@RequestMapping(value="/verify/{id}")
	public @ResponseBody void verify( HttpSession session,HttpServletResponse response,Model model){
		String text = producer.createText();
		BufferedImage image=producer.createImage(text);
		response.setHeader("Pragma", "No-cache");
		response.setHeader("Cache-Control", "No-cache");
		response.setDateHeader("Expires", -10); 		
		session.setAttribute("verifyCode",text );		
		ServletOutputStream os;
		try {
			os = response.getOutputStream();		
		response.setContentType("image/jpeg");
			ImageIO.write(image, "jpg", os);
			os.close();			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

}
