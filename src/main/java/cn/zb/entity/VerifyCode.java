package cn.zb.entity;

import java.awt.image.BufferedImage;

import org.springframework.stereotype.Component;
@Component
public class VerifyCode {
	private String verifyCode;
	private  BufferedImage img;
	
	public  BufferedImage getImg() {
		return img;
	}

	public void setImg(BufferedImage img) {
		this.img = img;
	}

	public String getVerifyCode() {
		return verifyCode;
	}

	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}
}
