	package cn.zb.config;

import java.util.UUID;

import javax.annotation.Resource;

import cn.zb.make.GetTime;

public class FilePath {
	@Resource
	private GetTime getTime;
	private String userHeadImagePath;
	public String getUserHeadImagePath() {
		return userHeadImagePath;
	}
	public void setUserHeadImagePath(String userHeadImagePath) {
		this.userHeadImagePath = userHeadImagePath+"\\"+getTime.createTime()+"\\"+UUID.randomUUID().toString();
	}
	public String getWorkerHeadImagePath() {
		return workerHeadImagePath;
	}
	public void setWorkerHeadImagePath(String workerHeadImagePath) {
		this.workerHeadImagePath = workerHeadImagePath+"\\"+getTime.createTime()+"\\"+UUID.randomUUID().toString();
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath+"\\"+getTime.createTime()+"\\"+UUID.randomUUID().toString();
	}
	private String workerHeadImagePath;
	private String filePath;
}
