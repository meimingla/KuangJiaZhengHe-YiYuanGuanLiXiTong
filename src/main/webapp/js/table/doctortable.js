function updata()
{
  document.getElementById('light').style.display = 'block';
  document.getElementById('fade').style.display = 'block';
  var s = $('#table').bootstrapTable('getSelections');

  document.getElementById('name').value = s[0].name;
  document.getElementById('age').value = s[0].age;
  document.getElementById('department').value = s[0].department;
  document.getElementById('address').value = s[0].address;
  document.getElementById('TEL').value = s[0].tel;
  document.getElementById('sex').value = s[0].sex;
  $('#put').click(function ()
  {
    var s = $('#table').bootstrapTable('getSelections');
    var id = s[0].id;
    var name = document.getElementById('name').value;
    var age = document.getElementById('age').value;
    var department = document.getElementById('department').value;
    var address = document.getElementById('address').value;
    var TEL = document.getElementById('TEL').value;
    var sex = document.getElementById('sex').value;
    $.ajax({
      type: 'POST',
      url: 'DispatchAction?method=updataDoctor',
      data: 'id=' + id + '&name=' + name + '&age=' + age + '&department=' + department + '&address=' + address + '&TEL=' + TEL + '&sex=' + sex,
      success: function (msg) {
        if (msg == 'true')
        {
          document.getElementById('light').style.display = 'none';
          document.getElementById('fade').style.display = 'none';
          $('#table').bootstrapTable('refresh');
          return true;
        } 
        else {
          alert('修改failuer');
          return false;
        }
      },
    });
  });
  return;
}
function insert() {
  document.getElementById('light2').style.display = 'block';
  document.getElementById('fade').style.display = 'block';
  var name = document.getElementById('name2').value;
  var age = document.getElementById('age2').value;
  var department = document.getElementById('department2').value;
  var address = document.getElementById('address2').value;
  var TEL = document.getElementById('TEL2').value;
  var sex = document.getElementById('sex2').value;
  $.ajax({
    type: 'POST',
    url: 'DispatchAction?method=insertDoctor',
    data: 'name=' + name + '&age=' + age + '&department=' + department + '&address=' + address + '&TEL=' + TEL + '&sex=' + sex,
    success: function (msg) {
      if (msg == 'true')
      {
        alert('添加成功');
        document.getElementById('light2').style.display = 'none';
        document.getElementById('fade').style.display = 'none';
        $('#table').bootstrapTable('refresh');
        return true;
      } 
      else {
        alert('添加失败了');
        return false;
      }
    },
  });
  return;
}
function delect()
{
  var s = $('#table').bootstrapTable('getSelections');
  var last = JSON.stringify(s);
  $.ajax({
    type: 'POST',
    url: 'DispatchAction?method=delectDoctor',
    data: 'json=' + last,
    success: function (msg) {
      if (msg) {
        $('#table').bootstrapTable('refresh');
      } 
      else {
        alert('删除失败!');
      }
    },
    // datatype:JSON
  });
}
