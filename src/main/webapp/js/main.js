/**
 * 
 */
 $("#verify").click(function(){$(this).attr("src","HelpTool/verify/" + Math.random() * 10);});
    $("input[type=file]").change(function(){$("div[div=head]").append("<img class='pull-right img-thumbnail '  width='50' height='50'  />");});
	//当文件input改变时，将图片URL传到第一个img
	  $("input[name=headImg]").change(function () {
	  	  	var fileObj = $('input[name=headImg]')[0];
	   	    var file = fileObj.files[0];
	        var windowURL = window.URL || window.webkitURL;
	        var dataURL;
	        var $img = $("img:first");
	     if (fileObj && fileObj.files && fileObj.files[0]) {
	         dataURL = windowURL.createObjectURL(file);
	      	$img.attr('src', dataURL);} 	  
	  });


	$(document).ready(	
		function(){
			$("#register").validate({
			  submitHandler:function(form){						 
			  var formElement = document.querySelector("#register");
			  var formData = new FormData(formElement);  
			  $.ajax({
           		 url: 'outLogin/register',
           		 type: 'POST',           		 
           		 data: formData,
           		 // 告诉jQuery不要去处理发送的数据
           		 processData: false,
          		  // 告诉jQuery不要去设置Content-Type请求头
          		 contentType: false,          		 
          		  success: function (msg) {
          		  		if(msg.status==100)
          		  			         		  				
          		  				$.growl.notice({
     					 title: "恭喜",
     						 message:msg.message
   						 }); 
          		  		if(msg.status==200) 
          		  			{	          		  			
          		  				$( "em" ).remove();  
          		  				$(  "#verifyCode" ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
								$( "#verifyCode" ).next( "span" ).remove();														          		  			          		  			
								$("<em class='help-block'>"+msg.message+"</em>").insertAfter( $( "#verifyCode" ) );													
          		  			}			
            	  		}
        	  	});
			 },
			 	 rules:{
			 	 headImg:{
			  	accept:true,
			    checkPic:true,
			    checkPicSize:true
			  	},
				name:{required:true,
						rangelength:[1,8]},					
				password:{required:true,
							rangelength:[6,20]},
				okpassword:{required:true,
							equalTo:"input[name=password]",},
				tel:{required:true,
							isPhone:true,
							},
				QQ:{required:true,
							isQQ:true,
							},
				email:{
							required:true,
							email:true,
							remote:"outLogin/verifyEmail"
							},
				verify:{required:true,},
			},			  
			 messages:{
				email:{
						remote:"邮箱已使用，请更换邮箱！"
						}
				
			},
			errorElement: "em",
				errorPlacement: function ( error, element ) {	
				$( "em" ).remove(); 			
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );
					// Add `has-feedback` class to the parent div.form-group
					// in order to add icons to inputs
					element.parents( ".col-sm-5" ).addClass( "has-feedback" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}

					// Add the span element, if doesn't exists, and apply the icon classes to it.
				//	if ( !element.next( "span" )[ 0 ] ) {
				//		$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
				//	}
				},
				success: function ( label, element ) {	
				    $( "em" ).remove(); 				
					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !$( element ).next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
					$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
				},
				unhighlight: function ( element, errorClass, validClass ) {				
					$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
					$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
				}
			
			}
				);
		}
		);
   var loading= "<div id='loading' > \
	   <div style='width:100%;height:100%;top:0px;left:0px;position:fixed;background:#636363;opacity:0.9;z-index:998;'></div>\
	   <div><img src='images/loading.gif' style='width:3rem;position:absolute;left:50%;top:50%;z-index:999;margin-left:-5rem;margin-top:-5rem;' /></div>\
	</div>";
	$(document).ajaxStart(function(){	
		$(loading).appendTo('body');		
	}).ajaxStop(function() {	
		$("#loading").remove();
		});