<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>在线咨询 page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<style type="text/css">
		.no-close .ui-dialog-titlebar-close {
 			 display: none;}
	</style>
	<link href="jsTools/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="jsTools/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="jsTools/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
     
     <script type="text/javascript" src="jsTools/jquery-ui-1.12.1.custom/jquery-ui.js"></script> 
    <link href="jsTools/jquery-ui-1.12.1.custom/jquery-ui.css" rel="stylesheet"></link> 
	
    <script type="text/javascript" src="js/websocket/consult.js"></script>
  </head>
  
  <body>
   <div class="container-fluid" style="background-color:#BBFFBB; ">
   
	<div class="row clearfix">
		<div id="List" class="col-md-3 column">
			
		</div>
		<div class="col-md-6 column" >
		<ul id='new_Message' class='list-group'>
			</ul>
			
		</div>
		<div class="col-md-3 column" >
		<div id="msg"></div>
		</div>
	</div>
</div>
  </body>
  <script type="text/javascript">
	
  
  /**
 * 
 */
 
//通过websocket 协议 向服务器发起全双工通信
  var dCount=new Object();//存放客户端打开的窗口数，不允许打开多个相同窗口	
  var user='${penson.user}';//当前用户名      
  //面板没打开时，消息的记录
	var no_msg=new Array();	
  var webSocket=null;
  $( function(){
  		
    		if(!window.WebSocket)
    	{ $("body").append("<h1>你的浏览器不支持WebSocket</h1>");return ;}
    		if(webSocket!=null){
    		return;
    		}
    	
    		var url = "ws://"+window.location.host+"/hospital_system/websocket?"+user;
    		webSocket=new WebSocket(url); 
    		webSocket.onerror = function(event) {
              alert("服务器地址不存在");
            };
            webSocket.onopen = function(event) {
            	
            	onOpen(event);
            };
            webSocket.onmessage = function(event) {            	
                onMessage(event);
            };   
           // webSocket.onclose=function(event){onClose(event);};		
		} );
		function onOpen(event) {
			 function showList(){
				$.get("Communication_userList.action",function(data,status){	
  			$("#List").html(data);
  	 		$("#List ul #"+user).remove();  });			
             }
           setInterval(showList, 500);    	 
		}
		//接收消息
		function onMessage(event) {
			var msg=JSON.parse(	event.data);
			if(msg.send==user)//如果是本人发送那么对话框是存在的，直接添加
				{  $("."+msg.accpet+" div ul").append("<li class='list-group-item'><div  style='background-color:#BBFFBB; margin-left:50%;margin-right:5%;'>"+msg.time+"</br>"+msg.message+"</div></li>"); 
					$(function(){$("#dialog div").animate({"scrollTop":$('#dialog div')[0].scrollHeight},"slow");});//调整滚动条
				}
				//不是本人，判断面板是否存在
			else{
			$('<audio id="chatAudio"> <source src="data/voice/3424.wav" type="audio/wav"> </audio>').appendTo('body');//载入声音文件 
			$("#chatAudio")[0].play();//播放消息提示音
					if($("."+msg.send).hasClass(msg.send))//窗口打开，消息直接添加
					{
				 $("."+msg.send+" div ul").append("<li class='list-group-item'><div  class='success'>"+msg.time+"</br>&nbsp"+msg.send+":"+msg.message+"</div></li>");		
					$(function(){$("#dialog div").animate({"scrollTop":$('#dialog div')[0].scrollHeight},"slow");});//调整滚动条
					}
					//窗口不再，消息存起来
					else{	
						no_msg[no_msg.length]=event.data;
						$("#new_Message").html("<li class='list-group-item' onclick='show_msg()' style='color:red'>有新的消息"+no_msg.length+",点击查看</li>");
					}
				}
		};
		//查看未看到的消息
		function show_msg()
		{
			for(var i=0;i<no_msg.length;i++)
			{var msg=JSON.parse(no_msg[i]);
				show_no_msg(msg.send,msg);	
			}
			 no_msg=[];//看完的消息清空
			$("#new_Message").html("");
		}
		//查看未看到的消息对话框
		function show_no_msg(x_id,msg)
		{
		    var name=x_id;
			for(var i in dCount)
    		{if(i==name)
    		{ $("."+msg.send+" div ul").append("<li class='list-group-item'><div  class='success'>"+msg.time+"</br>&nbsp"+msg.send+":"+msg.message+"</div></li>");return;}//如果存在对话框，则不添加
    		}  	
    	var html="<div id='dialog' class='"+name+"' style='overflow:visible' title='正在与"+name+"通话' ><div style='height:250px;background-color: ;overflow-y:scroll;'><ul class='list-group'><li class='list-group-item'><div  class='success'>"+msg.time+"</br>&nbsp"+msg.send+":"+msg.message+"</div></li></ul></div><input placeholder='回车键发送' id='msg' name='"+name+"' onkeydown='message_send(event,this)' type='text'/></div>"; 
    	$("#msg").append(html);
    	create_dialog(name);
    	dCount[name]=name;
    	$( "."+name).dialog( "open" ); 
		}
		function sendMessage(event)
		{
			
		}
		//打开对话框
   function send(x_id){
    	var name=x_id.id;  
    	for(var i in dCount)
    		{if(i==name)
    		{alert("您已打开对话");return;}
    		}  	
    	var html="<div id='dialog' class='"+name+"' style='overflow:visible' title='正在与"+name+"通话' ><div style='height:250px;background-color: ;overflow-y:scroll;'><ul class='list-group'></ul></div><input placeholder='回车键发送' name='"+name+"' id='aaa' onkeydown='message_send(event,this)' type='text'/></div>";
    	$("#msg").append(html);
    	create_dialog(name);
    	dCount[name]=name;
    	$( "."+name).dialog( "open" ); 
   }
   function message_send(event,obj)//发送消息,name的值兼容有问题
   {
   if(event.keyCode==13)
   {
   var message=obj.value;
   if(message=="")
   return;
   obj.value="";
   var send='${penson.user}';
   var accpet=obj.name;
   var date=new Date();
   var time=date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
   	webSocket.send("[{message:'"+message+"',send:'"+send+"',accpet:'"+accpet+"',time:'"+time+"'}]");
   }
   
   }
      
   //对话框的创建
    function create_dialog(id_name)
    {
    	$( "."+id_name ).dialog({
    	  dialogClass: "no-close",
   	      autoOpen: false,
   	      position:'right bottom',
   	      width:600,
   	      height:400,
   	      maxHeight:400,
   	  	  resizable: true,
   	  	  buttons: {
   	  	      
   	  	      "关闭": function() {delete dCount[id_name];$(this).remove();}
   	  	   },
   	   
   	      show: {
   	        effect: "blind",
   	        duration: 1000
   	      },
   	      hide: {
   	        effect: "explode",
   	        duration: 1000
   	      },
   	      enable:true,
   	    });  	 	 
    } 
    window.onbeforeunload=function(){ return "离开后您将无法咨询";};
    window.onunload=function() {webSocket.close();};

  </script>
</html>
