<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>密码找回</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="密码找回">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link href="jsTools/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="jsTools/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="jsTools/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    
    <script type="text/javascript" src="jsTools/jquery-ui-1.12.1.custom/jquery-ui.js"></script> 
    <link href="jsTools/jquery-ui-1.12.1.custom/jquery-ui.css" rel="stylesheet"></link> 
   
    <script src="jsTools/jquery-validation-1.16.0/dist/jquery.validate.min.js"></script>
    <script src="jsTools/jquery-validation-1.16.0/dist/localization/messages_zh.js"></script>
    <script src="jsTools/jquery-validation-1.16.0/dist/additional-methods.js"></script>
    
     <script src="jsTools/jquery.growl/javascripts/jquery.growl.js"></script>
    <link href="jsTools/jquery.growl/stylesheets/jquery.growl.css" rel="stylesheet"></link> 
  </head>
  
  <body>
  <div class="container">
	<div class="row clearfix" >
	<div class="col-sm-12"></div>
    <form class="form-horizontal" role="form" action="outLogin/getpassword" method="post">
    		<div class="form-group">
					 <label for="workerLogin" class="col-sm-4 control-label">邮箱	</label>
					<div class="col-sm-5">
						<input class="form-control"  name="email" type="text"  placeholder="请输入邮箱" />
					</div>
			</div>
			<div class="form-group">
					<div class="col-sm-offset-5 col-sm-4">
						 <button type="submit" class="btn btn-primary">找回密码</button>
					</div>
			</div>
    </form>
    </div>
   </div> 
  </body>
  <script type="text/javascript">
  $(document).ready(	
  	function(){
			$("form[role=form]").validate({
			  submitHandler:function(form){				
				 $.post("outLogin/getpassword",$("form").serialize(),function(msg){				
				 	if(msg.status==100){				 	
				 			$.growl.notice({
     					 title: "",
     						 message:msg.message
   						 }); 
				 	}
				 	else{ 
				 		$.growl.error({
     					 title: "",
     						 message:msg.message
   						 }); 
				 	}
				 });
			 },
			  rules:{
				email:{required:true,
				email:true,							
				}
			},			
			errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					// Add `has-feedback` class to the parent div.form-group
					// in order to add icons to inputs
					element.parents( ".col-sm-5" ).addClass( "has-feedback" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}

					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !element.next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
					}
				},
				success: function ( label, element ) {
					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !$( element ).next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
					$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
				},
				unhighlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
					$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
				}
			
			}
				);
		}
		);
		var mask = '<div id="hicap_mask" style="width:100%;height:100rem;top:0px;left:0px;position:fixed;background:#FFF;opacity:0.9;z-index:998;"></div>';
     var loading = '<div id="loading"><img src="images/loading.gif" style="width:10rem;position:absolute;left:50%;top:50%;z-index:999;margin-left:-5rem;margin-top:-5rem;" /></div>';
				   
 	$(document).ajaxStart(function(){
		$(mask).appendTo('body');
		$(loading).appendTo('body');
	}).ajaxStop(function() {
		$("#hicap_mask").remove();
		$("#loading").remove();
		});
  </script>
</html>
