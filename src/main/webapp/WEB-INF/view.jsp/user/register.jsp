<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>注册</title>
     <meta name="viewport" content="width=device-width,height=device-height,inital-scale=1.0,maximum-scale=1.0,user-scalable=no;">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<meta http-equiv="X-UA-Compatible" content="IE=9">
    <META HTTP-EQUIV="Cache-Control" CONTENT="no-cache, must-revalidate"> 
<!--     <META HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT"> -->
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	 <link type="text/css" rel="stylesheet" href="css/page/register.css" />
	
	<link href="jsTools/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="jsTools/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="jsTools/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    
    <script type="text/javascript" src="jsTools/jquery-ui-1.12.1.custom/jquery-ui.js"></script> 
    <link href="jsTools/jquery-ui-1.12.1.custom/jquery-ui.css" rel="stylesheet"></link> 
   
    <script src="jsTools/jquery-validation-1.16.0/dist/jquery.validate.min.js"></script>
    <script src="jsTools/jquery-validation-1.16.0/dist/localization/messages_zh.js"></script>
    <script src="jsTools/jquery-validation-1.16.0/dist/additional-methods.js"></script>
    
    <script src="jsTools/jquery.growl/javascripts/jquery.growl.js"></script>
    <link href="jsTools/jquery.growl/stylesheets/jquery.growl.css" rel="stylesheet"></link> 
    <style type="text/css">
    		input[type=file] {
    				  width: 144px;
   					  height: 41px;
  					  cursor: pointer;
  					  font-size: 30px;
 				      outline: medium none;
    				  position: absolute;
 					  filter:alpha(opacity=0);
  					  -moz-opacity:0;
  					  opacity:0; 
  					  left:0px;
  					  top: 0px;
    				}
    </style>
  </head>
 
  <body >
    <div class="container-fluid">
    	<div class="row clearfix">
    		<div class="col-sm-12">
    			<div class="panel page-header" style="background-image: url('css/images/body.jpg');text-align:center;margin-top: 0">
    				<h3 class="panel-title" id="logo"><b><i>注册页面</i></b></h3>
				</div>										
			</div>
			<div class="col-sm-8 col-sm-offset-2">						
    			<div class="panel panel-info" style="margin-top: 50">
    				<div class="panal-body">
    					<form class="form-Horizontal">    						 
			 				 <div class="form-group">	
								 <label for="headImg" class="col-sm-4 control-label">头像</label>
								 <div class="col-sm-5">								 
								 <div div="head"><button class="btn btn-primary btn-default"><span class="glyphicon glyphicon-picture">选择图片</span></button></div>		
           						 <input class="file"  accept="image/png,image/gif,image/bmp"  name="headImg" type="file"  />            						          						 										
								</div>
							</div>
							<div class="form-group">
								<label  class="col-sm-4 control-label">昵称</label>
			 					 <div class="col-sm-5">
									<input class="form-control"  name="name" type="text"  placeholder="昵称" />	
			 					 </div>
			 				 </div>
			 				 <div class="form-group">
								<label  class="col-sm-4 control-label">密码</label>
			 					 <div class="col-sm-5">
									<input class="form-control"  name="password" type="password"  placeholder="密码" />	
			 					 </div>
			 				 </div>
			 				 <div class="form-group">
								<label  class="col-sm-4 control-label">确认密码</label>
			 					 <div class="col-sm-5">
									<input class="form-control"  name="okpassword" type="password"  placeholder="确认密码" />	
			 					 </div>
			 				 </div>
			 				 <div class="form-group">
								<label  class="col-sm-4 control-label">QQ</label>
			 					 <div class="col-sm-5">
									<input class="form-control"  name="QQ" type="text"  placeholder="QQ" />	
			 					 </div>
			 				 </div>
			 				 <div class="form-group">
								<label  class="col-sm-4 control-label">手机号</label>
			 					 <div class="col-sm-5">
									<input class="form-control"  name="tel" type="text"  placeholder="手机号" />	
			 					 </div>
			 				 </div>
			 				<div class="form-group">
								<label  class="col-sm-4 control-label">邮箱</label>
			 					 <div class="col-sm-5">
									<input class="form-control"  name="email" type="text"  placeholder="邮箱" />	
			 					 </div>
			 				 </div>
			 				 <div class="form-group">
								<label  class="col-sm-4 control-label">验证码</label>
			 					 <div class="col-sm-5">
									<div class="col-sm-5">	<img  class="img-rounded"  title="点击刷新" width="120" height="30" id="verify" src="HelpTool/verify/1"></div>
					  				<div class="col-sm-offset-1 col-sm-6"><input class="form-control " id="verifyCode" name="verify" type="text" placeholder="验证码"/></div>	
			 					 </div>
			 				 </div>
			 				<div class="form-group">
								<div class="col-sm-offset-5 col-sm-4">
									 <button type="submit" class="btn btn-primary">注册</button>
									 <button type="reset" class="btn btn-defalut">清除</button>
								</div>
							</div>
    					</form>
    				</div>
    			</div>				
			</div>
    	</div>	
    </div>
    
    
  </body>
  <script type="text/javascript">
  	$("#verify").click(function(){$(this).attr("src","HelpTool/verify/" + Math.random() * 10);});
  	$("input[type=file]").change(function(){$("div[div=head]").append("<img class=' img-thumbnail '  width='140' height='140'  />");});
 	//当文件input改变时，将图片URL传到第一个img
	  $("input[name=headImg]").change(function () {
	  	  	var fileObj = $('input[name=headImg]')[0];
	   	    var file = fileObj.files[0];
	        var windowURL = window.URL || window.webkitURL;
	        var dataURL;
	        var $img = $("img:first");
	     if (fileObj && fileObj.files && fileObj.files[0]) {
	         dataURL = windowURL.createObjectURL(file);
	      	$img.attr('src', dataURL);} 	  
	  });
	  //表单验证
	  	$(document).ready(	
		function(){
			$("form").validate({
			  submitHandler:function(form){			  
			  var formElement = document.querySelector("form");
			  var formData = new FormData(formElement);  
			  $.ajax({
           		 url: 'outLogin/register',
           		 type: 'POST',           		 
           		 data: formData,
           		 // 告诉jQuery不要去处理发送的数据
           		 processData: false,
          		  // 告诉jQuery不要去设置Content-Type请求头
          		 contentType: false,
          		  success: function (msg) {
          		  		if(msg.status==100)
          		  			         		  				
          		  				$.growl.notice({
     					 title: "恭喜",
     						 message:msg.message
   						 }); 
          		  		if(msg.status==200) 
          		  			{	          		  			
          		  				$( "em" ).remove();  
          		  				$(  "#verifyCode" ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
								$( "#verifyCode" ).next( "span" ).remove();														          		  			          		  			
								$("<em class='help-block'>"+msg.message+"</em>").insertAfter( $( "#verifyCode" ) );													
          		  			}			
            	  		}
        	  	});
			 },
			 	 rules:{
			 	 headImg:{
			  	accept:true,
			    checkPic:true,
			    checkPicSize:true
			  	},
				name:{required:true,
						rangelength:[1,8]},					
				password:{required:true,
							rangelength:[6,20]},
				okpassword:{required:true,
							equalTo:"input[name=password]",},
				tel:{required:true,
							isPhone:true,
							},
				QQ:{required:true,
							isQQ:true,
							},
				email:{
							required:true,
							email:true,
							remote:"outLogin/verifyEmail.do"
							},
				verify:{required:true,},
			},			  
			 messages:{
				email:{
						remote:"邮箱已使用，请更换邮箱！"
						}
				
			},
			errorElement: "em",
				errorPlacement: function ( error, element ) {	
				$( "em" ).remove(); 			
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );
					// Add `has-feedback` class to the parent div.form-group
					// in order to add icons to inputs
					element.parents( ".col-sm-5" ).addClass( "has-feedback" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}

					// Add the span element, if doesn't exists, and apply the icon classes to it.
				//	if ( !element.next( "span" )[ 0 ] ) {
				//		$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
				//	}
				},
				success: function ( label, element ) {	
				    $( "em" ).remove(); 				
					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !$( element ).next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
					$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
				},
				unhighlight: function ( element, errorClass, validClass ) {				
					$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
					$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
				}
			
			}
				);
		}
		);
	  var mask = '<div id="hicap_mask" style="width:100%;height:100rem;top:0px;left:0px;position:fixed;background:#FFF;opacity:0.9;z-index:998;"></div>';
     var loading = '<div id="loading"><img src="images/loading.gif" style="width:10rem;position:absolute;left:50%;top:50%;z-index:999;margin-left:-5rem;margin-top:-5rem;" /></div>';
				   
 	$(document).ajaxStart(function(){
		$(mask).appendTo('body');
		$(loading).appendTo('body');
	}).ajaxStop(function() {
		$("#hicap_mask").remove();
		$("#loading").remove();
		});
	  
  </script>
</html>
