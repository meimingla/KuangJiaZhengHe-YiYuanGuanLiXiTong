<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>在线预约</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
   	<link href="jsTools/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="jsTools/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="jsTools/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
		
	<script type="text/javascript" src="jsTools/address_select/distpicker.data.js"></script>
    <script type="text/javascript" src="jsTools/address_select/distpicker.js"></script>
    
    <script type="text/javascript" src="jsTools/jquery-ui-1.12.1.custom/jquery-ui.js"></script> 
    <link href="jsTools/jquery-ui-1.12.1.custom/jquery-ui.css" rel="stylesheet"></link> 
   
    <script src="jsTools/jquery-validation-1.16.0/dist/jquery.validate.min.js"></script>
    <script src="jsTools/jquery-validation-1.16.0/dist/localization/messages_zh.js"></script>
    <script src="jsTools/jquery-validation-1.16.0/dist/additional-methods.js"></script>
    
     <script src="jsTools/jquery.growl/javascripts/jquery.growl.js"></script>
    <link href="jsTools/jquery.growl/stylesheets/jquery.growl.css" rel="stylesheet"></link> 
    
	<script type="text/javascript" src="js/page/onlineOrder.js"></script>
	
	
  </head>
  
  <body>
  <div class="container-fluid" style=" background-color:#cde6c7">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<form class="form-horizontal" id="patient" enctype="application/x-www-form-urlencoded" action="DispatchAction.action?method=insertPatient" method="post">
							<div class="form-group">
							 <label class="col-sm-4 control-label">姓名</label>
							<div class="col-sm-5">
								<input class="form-control" name="p_name"  type="text" placeholder="姓名"/>
							</div>
						</div>
						<div class="form-group">
							 <label  class="col-sm-4 control-label">年龄</label>
							<div class="col-sm-5">
								<input class="form-control" name="p_age" type="text" placeholder="年龄"/>
							</div>
						</div>
						<div class="form-group">
							 <label  class="col-sm-4 control-label ">性别</label>
							<div class="col-sm-5">
								<select name="p_sex" class="form-control">
								<option value="男">男</option>
								<option value="女">女</option></select>
							</div>
						</div>
						<div class="form-group">
							 <label  class="col-sm-4 control-label">地址</label>
							<div class="col-sm-5">
								<div data-toggle="distpicker">
        						<select  data-province="---- 选择省 ----" name="province"></select>
      							<select data-city="---- 选择市 ----" name="city"></select>
       							<select  data-district="---- 选择区 ----" name="district"></select>
   						  		 </div>
    						 </div>
						</div>
						<div class="form-group">
							 <label  class="col-sm-4 control-label">电话</label>
							<div class="col-sm-5">
								<input class="form-control" name="p_tel_num" type="text" placeholder="手机号"/>
							</div>
						</div>
						<div class="form-group">
							 <label  class="col-sm-4 control-label">科室</label>
							<div class="col-sm-5">
								<select class="form-control" name="p_out_type" title="请选择科室" >	
								</select>
							</div>
						</div>
						<div class="form-group">
							 <label  class="col-sm-4 control-label">治疗时间</label>
							<div class="col-sm-5">
								<input class="form-control" name="p_treat_time"  id="datepicker" type="text" placeholder="请输入预约日期"/>
							</div>
						</div>
						<div class="form-group">
							 <label  class="col-sm-4 control-label">身份证编号</label>
							<div class="col-sm-5">
								<input class="form-control" name="p_card_num" type="text" placeholder="身份证号"/>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-offset-4 col-sm-8">
								 <button type="submit" id='p-up' class="btn btn-default">预约</button>
								  <button type="reset" id='' class="btn btn-default">重置</button>
							</div>
						</div>
					</form>
			<div class="mes"></div>
		</div>
	</div>
</div>
  </body>
  <script type="text/javascript">
 $( "#datepicker" ).datepicker({minDate:0, maxDate: "+1M",showAnim: 'slideDown',firstDay : 1,nextText : '下一月',
	prevText : '上一月', });
  	
  	$("select[name=p_out_type]").one("click",function(){
    $.get('InsideDispacthAction.action?method=getOutpatientList',function(data,status)
  		{var s=JSON.parse(data);
  		$.each(s,function(k,v){
  			$("select[name=p_out_type]").append("<option value='"+v.id+"' >"+v.name+"</option>");
  			}); 
  		});
  });
  
  $(document).ready(	
		function(){
			$("#patient").validate({
			  submitHandler:function(form){
				form.submit();	
			 },
			  rules:{
				p_name:{required:true,
				rangelength:[2,7],
				},
				p_age:{required:true,
				digits:true,
				maxlength:3},
				p_sex:{required:true},
				province:{required:true},
				p_tel_num:{required:true,
				isPhone:true},
				p_out_type:{required:true},
				p_treat_time:{required:true,
				 	dateISO:true},
				p_card_num:{required:true,
				 isIdCardNo:true
				}		
			},
			errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					// Add `has-feedback` class to the parent div.form-group
					// in order to add icons to inputs
					element.parents( ".col-sm-5" ).addClass( "has-feedback" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}

					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !element.next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
					}
				},
				success: function ( label, element ) {
					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !$( element ).next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
					$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
				},
				unhighlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
					$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
				}
			
			}
				);
		}
		);
 </script>
</html>
