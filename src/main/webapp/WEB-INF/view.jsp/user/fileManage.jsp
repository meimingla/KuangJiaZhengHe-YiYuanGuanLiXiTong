<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>文件管理 page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="文件管理">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link href="jsTools/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="jsTools/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="jsTools/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    
    <script type="text/javascript" src="js/diyUpload/js/diyUpload.js"></script>
    <script type="text/javascript" src="js/diyUpload/js/webuploader.html5only.min.js"></script>
    <link rel="stylesheet" type="text/css" href="js/diyUpload/css/diyUpload.css"/> 
	<link rel="stylesheet" type="text/css" href="js/diyUpload/css/webuploader.css"/> 

	
  </head>
  
  <body>
  	<div  class="container-fluid">
       
		<div class="row clearfix">
		<div class="col-md-8 column well well-lg">
		
			<table class="table-hover" id="fileList">
			
			</table>
	
			
		</div>
		<div class="col-md-4 column" >
   				<div id="as"></div>	
		</div>
		</div>
	</div>
  <script type="text/javascript">
   
    $.get("DispatchAction.action?method=getFile",function(data,status){
    	for(var i=0;i<data.length;i++)
    	{	
    		var tr="<tr><td><a title="+data[i].name+" style='max-width:350;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;float:left;text-decoration: none;' href='"+data[i].path+"'>"+"<span class='glyphicon glyphicon-download-alt'></span>"+data[i].name+"</a></td>&nbsp&nbsp<td><span class='label label-info'>文件大小:"+data[i].size+"</span></td><td>&nbsp&nbsp贡献者:"+data[i].username+"</td></tr>";
    		$("#fileList").append(tr);
    	}
       });
 	
/*
* 服务器地址,成功返回,失败返回参数格式依照jquery.ajax习惯;
* 其他参数同WebUploader
*/
 $('#as').diyUpload({
	url:'file/upload.do',
	success:function( data ) {
		$("#fileList").empty();
		$.get("DispatchAction.action?method=getFile",function(data,status){
    	for(var i=0;i<data.length;i++)
    	{    
    		var tr="<tr><td><span>点击下载：</span><a href='"+data[i].path+"'>"+data[i].name+"</a>&nbsp&nbsp<span class='label label-info'>文件大小:"+data[i].size+"</span></td><td>&nbsp&nbsp贡献者:"+data[i].username+"</td></tr>";
    		$("#fileList").append(tr);
    	}
       });
	},
	error:function( err ) {
		alert("上传失败!");	
	},
	buttonText : '上传文件',
    chunked:false,
	// 分片大小
	//chunkSize:512 * 1024,
	//最大上传的文件数量, 总文件大小,单个文件大小(单位字节);
	fileNumLimit:10,
	fileSizeLimit:1500 * 1024 * 1024,
	fileSingleSizeLimit:150 * 1024 * 1024,
	accept: {title:"application/zip",
				extensions:"zip,rar",
				mimeTypes:"application/zip"}
});


</script>
    
  </body>
</html>
