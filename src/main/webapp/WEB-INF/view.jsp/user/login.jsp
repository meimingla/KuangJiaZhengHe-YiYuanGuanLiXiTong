<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
 			 <form  class="form-Horizontal" >	
  				<div class="modal-header" style="background-color: ">
							<button type="button" class="close active" data-dismiss="modal" aria-hidden="true" >
								&times;
							</button>
							<h4 class="modal-title" >
								请登录
							</h4>
						</div>	
				<div class="modal-body">																														
								<div class="form-group">
									 <br>
									 <label for="workerLogin" class="col-sm-4 control-label">邮箱</label>
									<div class="col-sm-5">
										<input class="form-control"  name="email" type="text"  placeholder="邮箱" />
									</div>
								</div>
								
								<div class="form-group">
										
									 <label for="workerPassword" class="col-sm-4 control-label">密码</label><a href="Main/forget">忘记密码</a>
									<div class="col-sm-5">
										<input class="form-control "  name="password" type="password" placeholder="密码"/>
									</div>
								</div>	
								<div class="form-group">
									<div id="back" class="col-sm-4 col-sm-offset-4">
																				
									</div>
								</div>					
				</div>		
				<div class="modal-footer  alert-success" >
								<button type="submit" class="btn btn-primary">登录</button>
								<button type="reset" id="cancellation" class="btn btn-primary">重置</button>							
				</div>	
				</form>																		
  <script type="text/javascript">
  	$(document).ready(	
		function(){
			$(" form").validate({
			  submitHandler:function(form){
			  var data=$('form').serialize();
			   $.post("outLogin/login",data,function(msg){
			   	if(msg.status==200)
			   		{	$("#back").empty() ;
			   			$("#back").prepend("<div class='alert alert-danger'>"+msg.message+"</div>");
			   		}
			   		else{		   		
			   			 $('.signOne',parent.document).modal('hide');				   				   		
						 window.location.reload(false);			   			 			   		
			   		}			   		
			   		 }
			   );
			 },
			  rules:{
				email:{required:true,
				email:true,				
				},
				password:{required:true,
				},
			},
			errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					// Add `has-feedback` class to the parent div.form-group
					// in order to add icons to inputs
					element.parents( ".col-sm-5" ).addClass( "has-feedback" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}

					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !element.next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
					}
				},
				success: function ( label, element ) {
					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !$( element ).next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
					$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
				},
				unhighlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
					$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
				}			
			}
				);
		}
		);
  </script>
  
