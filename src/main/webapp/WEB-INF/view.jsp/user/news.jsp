<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>首页</title>    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	 <meta name="viewport" content="width=device-width,height=device-height,inital-scale=1.0,maximum-scale=1.0,user-scalable=no;">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link href="jsTools/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="jsTools/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="jsTools/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
  </head>
  
  <div class="container">
	<div class="row clearfix">
		<div class="col-md-10 col-md-push-1 column">
			<div id="myCarousel" class="carousel slide">
	<!-- 轮播（Carousel）指标 -->
	<ol class="carousel-indicators">
		<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		<li data-target="#myCarousel" data-slide-to="1"></li>
		<li data-target="#myCarousel" data-slide-to="2"></li>
	</ol>   
	<!-- 轮播（Carousel）项目 -->
	<div class="carousel-inner">
		<div class="item active">
			<img src="images/default.jpg" alt="First slide">
		</div>
		<div class="item">
			<img src="images/default1.jpg" alt="Second slide">
		</div>
		<div class="item">
			<img src="images/default2.jpg" alt="Third slide">
		</div>
	</div>
	<!-- 轮播（Carousel）导航 -->
	<a class="carousel-control left" href="#myCarousel" 
	   data-slide="prev">&lsaquo;</a>
	<a class="carousel-control right" href="#myCarousel" 
	   data-slide="next">&rsaquo;</a>
</div> 
				
			</div> 
			<div class="row clearfix">
				<div class="col-md-4 column">
				
				</div>
				<div class="col-md-4 column">
				<table class="table table-striped">
					
				<tbody>
				<c:forEach items="${news}" var="news" varStatus="status" >
					<tr class="info">
						<td>
							<span class="glyphicon glyphicon-eye-open"></span></td><td>&nbsp&nbsp<a target="_blank" title="${news.title}" style="max-width:120;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;float:left;text-decoration: none;color: black;"   href="">${news.title}</a></td>&nbsp&nbsp 
						</td>
						<td>${news.writeTime}</td>	
					</tr>
			   </c:forEach>
				</tbody>
			</table>
				</div>
				<div class="col-md-4 column">
				</div>
	<div class="row clearfix">
		<div class="col-md-10 col-md-push-4 column">
			 <ul class="pagination" id="box" >
				<li>
					 <a href="#">Prev</a>
				</li>
				<li>
					 <a href="#">1</a>
				</li>
				<li>
					 <a href="#">2</a>
				</li>
				<li>
					 <a href="#">3</a>
				</li>
				<li>
					 <a href="#">4</a>
				</li>
				<li>
					 <a href="#">5</a>
				</li>
				<li id="fenye">
					 <a href="#">Next</a>
				</li>
			</ul>
		</div>
	</div>
		</div>
	</div>
</div>
  </body>
</html>