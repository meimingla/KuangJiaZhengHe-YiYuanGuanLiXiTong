<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
     <title>医院管理系统</title>    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,height=device-height,inital-scale=1.0,maximum-scale=1.0,user-scalable=no;">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link href="jsTools/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="jsTools/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="jsTools/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    
    <script type="text/javascript" src="jsTools/jquery-ui-1.12.1.custom/jquery-ui.js"></script> 
    <link href="jsTools/jquery-ui-1.12.1.custom/jquery-ui.css" rel="stylesheet"></link> 
   
    <script src="jsTools/jquery-validation-1.16.0/dist/jquery.validate.min.js"></script>
    <script src="jsTools/jquery-validation-1.16.0/dist/localization/messages_zh.js"></script>
    <script src="jsTools/jquery-validation-1.16.0/dist/additional-methods.js"></script>
    <style type="text/css">
    	#index{
    	width:100%;
    	position:fixed;
    	z-index: 1;
    	}
    </style>
  </head>
 	<body > 	
 	
 		<div class="container-fluid " id="index" style="background-color: white;">
 			<div  class=" row clearfix " > 	
 				<div class="col-sm-5 column col-sm-offset-7 ">							
 				<ul class="nav nav-pills nav-justified" style="float: right;">
				<li id="sign">
				<c:if test="${user.name==null}">				
					<a class="btn.btn-info btn-sm"  data-keyboard="true"  href="Main/login"  data-toggle="modal"  data-target=".signOne" title="请登录">登录
					</a>
				</c:if>
				<c:if test="${user.name!=null}">
				<img class="img-circle " src="${user.headImage}" width="50" height="50" >
				<h4><span class="label label-info">昵称：${user.name}</span></h4>
				</c:if>	 
					 
				</li>
				<li>
					 <a href="Main/register">注册</a>
				</li>
				<li>
					 <a href="Main/forget">忘记密码</a>
				</li>
				<li >
					 <a  href="#">信息</a>
				</li>
				<li>
					 <a href="outLogin/exit">退出</a>
				</li>
				<li>
					 <a href="workerSign">工作人员入口</a>
				</li>
				</ul>	
				
				</div>			
 			</div>
 		</div>
 	 
	<div class="col-sm-12" style="background-color:#6495ED ;margin-top: 85">
		<div class="col-sm-4 col-sm-offset-4" >
		<ul class="nav nav-pills btn-group title" >
				<li   class="active" >			
					 <a  href="javascript:change_fame('Main/news')" >首页</a>
				</li>
				<li  class="active" >
					 <a href="javascript:change_fame('Main/forum')" >进入论坛</a>
				</li>
				<li  class="active" >
					 <a href="javascript:change_fame('Main/fileManage')" >文件管理</a>
				</li>
				<li  class="active"  >
					 <a href="javascript:change_fame('Main/onlineChat)"  >在线咨询</a>
				</li>
				<li  class="active"  >
					 <a href="javascript:change_fame('Main/onlineOrder')" >在线预约</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="modal fade signOne"  role="dialog">
		<div class="modal-dialog" >									
					<div class="modal-content">
						
					</div>						   			 
		</div>
	</div>
	<div class="row clearfix" style="color:  #CAE1FF"">
		<div class="col-md-12 column">
		<iframe frameborder="0"  id="fame" src="Main/news.do"  height="100%" width="100%"></iframe>
		</div>
	</div>
	

	</body>
	<script type="text/javascript">		
	  $(".title li").mouseenter(function (){ $(this).removeClass("active");});
	  $(".title li").mouseleave(function (){ $(this).addClass("active");});
	function change_fame(src)
{
	$("iframe").attr("src",src);
}
	$(document).ready(function(){
			$("#get_head").click(function(){window.location.hash="#head";});
	});
	var mask = '<div id="hicap_mask" style="width:100%;height:100rem;top:0px;left:0px;position:fixed;background:#FFF;opacity:0.9;z-index:998;"></div>';
     var loading = '<div id="loading"><img src="images/loading.gif" style="width:10rem;position:absolute;left:50%;top:50%;z-index:999;margin-left:-5rem;margin-top:-5rem;" /></div>';
				   
 	$(document).ajaxStart(function(){
		$(mask).appendTo('body');
		$(loading).appendTo('body');
	}).ajaxStop(function() {
		//$("#hicap_mask").remove();
		//$("#loading").remove();
		});
</script>
</html>
