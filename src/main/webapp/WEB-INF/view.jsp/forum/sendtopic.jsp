<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>发帖 page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="帖子发表">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link href="jsTools/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="jsTools/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="jsTools/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    
     <script type="text/javascript" src="js/forum/sendreply.js"></script>
     <link href="jsTools/umeditor1.2.3/themes/default/css/umeditor.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="jsTools/umeditor1.2.3/third-party/jquery.min.js"></script>
    <script type="text/javascript" src="jsTools/umeditor1.2.3/third-party/template.min.js"></script>
    <script type="text/javascript" src="jsTools/umeditor1.2.3/lang/zh-cn/zh-cn.js"></script>
    <script type="text/javascript" charset="utf-8" src="jsTools/umeditor1.2.3/umeditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="jsTools/umeditor1.2.3/umeditor.min.js"></script>
    
    <link rel="stylesheet" type="text/css" href="jsTools/jquery-easyui-1.5.1/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="jsTools/jquery-easyui-1.5.1/themes/icon.css">
    <script type="text/javascript" src="jsTools/jquery-easyui-1.5.1/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="jsTools/jquery-easyui-1.5.1/locale/easyui-lang-zh_CN.js"></script>
    
     <script src="jsTools/jquery-validation-1.16.0/dist/jquery.validate.min.js"></script>
    <script src="jsTools/jquery-validation-1.16.0/dist/localization/messages_zh.js"></script>
    <script src="jsTools/jquery-validation-1.16.0/dist/additional-methods.js"></script>
    
     <script src="jsTools/jquery.growl/javascripts/jquery.growl.js"></script>
    <link href="jsTools/jquery.growl/stylesheets/jquery.growl.css" rel="stylesheet"></link> 
    
     
  </head>
  
  <body>
   <div class="container well">
 					 <div class="panal-body" style="background-color: #ACD6FF;  margin-top: 2%"> 
    					<form class="form-Horizontal">    	 			 				 
							<div class="form-group control-group">
								<label  class="col-sm-1 control-label">类别</label>
			 					 <div class="col-sm-10">
									<input name="sId" type="text" class="form-control" id="section" >
			 					 </div>
			 				 </div>
			 				 <div class="form-group control-group" >
								<label  class="col-sm-1 control-label">标题</label>
			 					 <div class="col-sm-10">
									<input class="form-control"  name="title" type="text"  placeholder="标题" />	
			 					 </div>
			 				 </div>			 				
			 				  <div class="form-group">
			 				  	<label  class="col-sm-1 control-label">内容</label>
			 					 <div class="col-sm-10">
									<script type="text/plain" id="content" class="form-control" name="content" style="height:300;width:100%">  					 
									</script>	
			 					 </div>
			 				  </div>
			 				 <div class="form-group">
								<label  class="col-sm-1 control-label">验证码</label>
			 					 <div class="col-sm-10">
									<div class="col-sm-5">	<img  class="img-rounded"  title="点击刷新" width="120" height="30" id="verify" src="HelpTool/verify/1"></div>
					  				<div class="col-sm-offset-1 col-sm-6"><input class="form-control " id="verifyCode" name="verify" type="text" placeholder="验证码"/></div>	
			 					 </div>
			 				 </div>
			 				<div class="form-group">
								<div class="col-sm-offset-5 col-sm-4">
									 <button type="submit" class="btn btn-primary">发表</button>
									 <button type="reset" class="btn btn-defalut">清除</button>
								</div>
							</div>
    					</form>
    					</div>	
   </div>

   		  
  </body>
  <script type="text/javascript">
  	$("#verify").click(function(){$(this).attr("src","HelpTool/verify/" + Math.random() * 10);});
    $(document).ready(	
		function(){
			$(" form").validate({
			  submitHandler:function(form){		
				  var verify =$("#section").val();	  
				  var content =$("#content").html();
				  if(verify==''){
				  alert("请选择类型"); return}		;
				   if(content==''){
				  alert("请填写文章"); return}	
				var s=  $(form).serialize();
				  	
				  $.post('Forum/publishTopic',s, function(msg){
				  		if(msg.status==100) {         		  			         		  				
          		  				$.growl.notice({
     					 title: "恭喜",
     						 message:msg.message
   						 });
   						 window.location.href="Main/forum"; 
   						 }
          		  		if(msg.status==200) 
          		  			{	          		  			
          		  				$( "em" ).remove();  
          		  				$(  "#verifyCode" ).parents( ".col-sm-10" ).addClass( "has-error" ).removeClass( "has-success" );
								$( "#verifyCode" ).next( "span" ).remove();														          		  			          		  			
								$("<em class='help-block'>"+msg.message+"</em>").insertAfter( $( "#verifyCode" ) );													
          		  			}			
				  	
				  });
			 },
			  rules:{				
				title:{required:true,
				},				
				verify:{required:true,}
			},
			errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );
					$( "em" ).remove(); 
					// Add `has-feedback` class to the parent div.form-group
					// in order to add icons to inputs
					element.parents( ".col-sm-10" ).addClass( "has-feedback" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}

					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !element.next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
					}
				},
				success: function ( label, element ) {
					// Add the span element, if doesn't exists, and apply the icon classes to it.
					$( "em" ).remove(); 
					if ( !$( element ).next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-10" ).addClass( "has-error" ).removeClass( "has-success" );
					$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
				},
				unhighlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-10" ).addClass( "has-success" ).removeClass( "has-error" );
					$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
				}			
			}
				);
		
		
		$(function() {
		    $('#section').combotree({
		        url: 'Forum/section',
		        onBeforeSelect: function(node) {
		            if (!$(this).tree('isLeaf', node.target)) {
		                return false;
		            }
		        },
		        onClick: function(node) {
		            if (!$(this).tree('isLeaf', node.target)) {
		                $('#section').combo('showPanel');
		            }
		        }
	   		 });
		}) ;
   var um = UM.getEditor('content');
		
		
		});
  </script>
</html>
