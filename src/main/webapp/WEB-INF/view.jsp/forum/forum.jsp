<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="cn.zb.service.*" %>
<%@ page import="cn.zb.pojo.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>医院论坛</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="医院论坛">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
    <link href="jsTools/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="jsTools/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="jsTools/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    
    <script src="jsTools/jquery-validation-1.16.0/dist/jquery.validate.min.js"></script>
    <script src="jsTools/jquery-validation-1.16.0/dist/localization/messages_zh.js"></script>
    <script src="jsTools/jquery-validation-1.16.0/dist/additional-methods.js"></script>
   
     <script src="jsTools/jquery.growl/javascripts/jquery.growl.js"></script>
    <link href="jsTools/jquery.growl/stylesheets/jquery.growl.css" rel="stylesheet"></link> 
    
    <link rel="stylesheet" type="text/css" href="jsTools/jquery-easyui-1.5.1/themes/bootstrap/easyui.css"> 
    <script type="text/javascript" src="jsTools/jquery-easyui-1.5.1/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="jsTools/jquery-easyui-1.5.1/locale/easyui-lang-zh_CN.js"></script>
   	<!-- 分页条 --> <!-- 加在easy ui之后 重写easy ui的方法 -->   	 
    
   	<script type="text/javascript" src="jsTools/bootStrapPager/js/extendPagination.js"></script>
   	<style>

	th,td {
  text-align: center;}

</style>
  </head>
  
  <body >
   <div class="container-fluid">
		
			<div class="col-sm-2 col-sm-offset-1" >
			  <div class="row clearfix" >
		    	<div class="easyui-panel" style="padding:5px;background-color: #C4E1FF; height:840">			    	
	    			<ul class="easyui-tree" 
					data-options="
					url:'Forum/section',
					method:'post',
					animate:true,					
					formatter:function(node){
						if(node.children!='')
						{
							return node.text+'&nbsp(<span  style=\'color:blue\'>'+node.children.length+'</span>)';
						}
						else{
							return '<a href=javascript:getAll(\'\',\'Forum/sectionTopic/'+node.id+'\',\'Forum/pageSectionTopic/'+node.id+'\') > '+node.text+'</a>&nbsp<span class=\'label label-info \' style=\'font-size:9;padding-bottom:2;\'>帖子数：'+node.topics.length+'</span>'
						}																			
					}">
				</ul>
				</div>
   			  </div>  	 			
		  </div>
			<div class="col-sm-8 well" style="background-color: #FFE6D9" >								
				<div class="col-sm-6 col-sm-offset-6"> 
					<ul class="nav nav-pills btn-group title">
						<li><form id="search">
						    <div class="input-group ">
					            <input type="text" name="title" class=" form-control"  placeholder="搜索"  />
					            <span class="input-group-btn"> <button type="submit" class="btn btn-primary "><span class="glyphicon glyphicon-search"></span></button></span>
					        </div>
					        </form></li>
					    <c:if test="${user.name==null}">
						     <li style="margin-left: 10"> <button type="button" class="btn btn-primary easyui-tooltip"   disabled="disabled" title="请登录" id="open">发帖</button></li>
						</c:if>
						<c:if test="${user.name!=null}">
						     <li class="active" style="margin-left: 10"><form action="Forum/publish" method="post"><button type="submit" class="btn btn-primary">发帖</button> </form></li>	
						</c:if>
						
					</ul>  
					 
				</div>	
				
				<div class="col-sm-12 " id="topic">							
					<div class="table-responsive">
  						<table class="table table-striped table-hover"  align="center">
  						<thead class="active"><tr>
					        <th>作者</th>
					        <th>标题</th>
					        <th>时间</th>
					        <th>管理</th></tr></thead>
					        <tbody></tbody>					
					   </table>
					<div id="callBackPager" class="text-center" style="font-size: 16;" >
						
					</div>				
				</div>
			</div>
   </div> 
     
  </body>
  	
  <script type="text/javascript">	
  	
  //删除帖子
 		 function delect(id,name){ 		
 			 $.post('Forum/delect',{id:id,name:name},function(data){
 				alert(data.message);
 				window.location.reload(false);
 			});
 		}
 		//修改帖子
 		var update=function(id,name){
 			$.post('Forum/update',{id:id,name:name},function(data){ 	
 				console.log(data.message);		
 				if(data.status==100)
 					window.location.href=data.message+id;
 				else
 					alert(data.message);
 			});
 		};	
 	//分页查询
 	var pageSearch=function(url,parameter,page,limit){
 					$("table tbody").empty();
		 			$.post(url,parameter+"&page="+page+"&limit="+limit,function(data){ 				 				
		 				 $.each(data,function(k,v){  					 			
		 					 $("table tbody").append('<tr  class=\'success\'><td>'+v.user.name+'</td><td><a href=\'Forum/topic/'+v.id+'\' target=\'_blank\' style=\'width:100%;max-width:400;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;float:left;text-decoration: none;\' >'+v.title+'</a></td><td>'+v.publish+'</td><td><div class=\'dropdown\'><a href=\'#\' class=\'dropdown-toggle\' data-toggle=\'dropdown\'>管理<strong class=\'caret\'></strong></a><ul class=\'dropdown-menu\'><li><a class=\'active\' onclick=\'delect('+v.id+',"'+v.user.name+'")\'>删除</a><a class=\'active\' update('+v.id+',"'+v.user.name+'")\'>编辑</a></li></ul></div></td></tr>');
		 					}); 				
		 			});		
 			   };
 	//初始查询
 	var getAll=function(parameter,initUrl,initPageUrl)
 	{     var limit=5;
 			$.getJSON(initUrl,parameter,function(data){ 					
	 				$('#callBackPager').extendPagination({				
					            totalCount: data.length,				
					            showCount: 10,					
					            limit: limit,				
					            callback: function (page, limit) {						            				            						
									 pageSearch(initPageUrl,parameter,page,limit);
					            }
					 		});		
					 $("table tbody").empty();
					 if(data.length<1)
					 	 $("table tbody").append("<div class='alert alert-info'>无查找数据</div>");	
 					 $.each(data,function(k,v){  	 					  
 					 if(k>limit-1)
 					 return;				
 					 $("table tbody").append('<tr class=\'success\'><td>'+v.user.name+'</td><td><a href=\'Forum/topic/'+v.id+'\' target=\'_blank\' style=\'width:100%;max-width:400;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;float:left;text-decoration: none;\' >'+v.title+'</a></td><td>'+v.publish+'</td><td><div class=\'dropdown\'><a href=\'#\' class=\'dropdown-toggle\' data-toggle=\'dropdown\'>管理<strong class=\'caret\'></strong></a><ul class=\'dropdown-menu\'><li><a class=\'active\' onclick=\'delect('+v.id+',"'+v.user.name+'")\'>删除</a><a class=\'active\' onclick=\'update('+v.id+',"'+v.user.name+'")\'>编辑</a></li></ul></div></td></tr>');
 					});
 				});
 	}
 	$(function (){	
 			getAll('',"Forum/topic","Forum/pageTopics");
 		});
	 var mask = '<div id="hicap_mask" style="width:100%;height:100rem;top:0px;left:0px;position:fixed;background:#FFF;opacity:0.9;z-index:998;"></div>';
     var loading = '<div id="loading"><img src="images/loading.gif" style="width:10rem;position:absolute;left:50%;top:50%;z-index:999;margin-left:-5rem;margin-top:-5rem;" /></div>';
				   
 	$(document).ajaxStart(function(){
		$(mask).appendTo('body');
		$(loading).appendTo('body');
	}).ajaxStop(function() {
		$("#hicap_mask").remove();
		$("#loading").remove();
		});
		$(document).ready(	
	function(){
			$("#search").validate({
			  submitHandler:function(form){			  
			 var data=$(form).serialize();			 
			 getAll(data,"Forum/likeTopic","Forum/pageLikeTopic");
			 },
			 	 rules:{
				title:{required:true},
			},			  			
			errorElement: "em",
				errorPlacement: function ( error, element ) {	
				$( "em" ).remove(); 			
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );
					// Add `has-feedback` class to the parent div.form-group
					// in order to add icons to inputs
					element.parents().addClass( "has-feedback" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}

					// Add the span element, if doesn't exists, and apply the icon classes to it.
				//	if ( !element.next( "span" )[ 0 ] ) {
				//		$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
				//	}
				},
				success: function ( label, element ) {	
				    $( "em" ).remove(); 				
					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !$( element ).next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents().addClass( "has-error" ).removeClass( "has-success" );
					$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
				},
				unhighlight: function ( element, errorClass, validClass ) {				
					$( element ).parents(  ).addClass( "has-success" ).removeClass( "has-error" );
					$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
				}
			
			}
				);
		}
		);
 	
  </script>
</html>
