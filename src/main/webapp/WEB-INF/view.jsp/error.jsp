<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>错误提示</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="页面不存在">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
			<style type="text/css">
			body,ul,ol,li,p,dt,dl,dd,h1,h2,h3,h4,h5,h6,form,fieldset,table,td,th,img,div{margin:0;padding:0}
			img { border:0}
			body{background:#fff;color:#333;font-size:12px;font-family:Arial,'宋体'}
			ul,ol{list-style:none}
			h1,h2,h3,h4,h5,h6,em,stone,i{font-size:12px;font-weight:400}
			select,input,select{vertical-align:middle}
			a:link,a:visited{color:#333;text-decoration:none}
			a:hover,a:active,a:focus{color:#f60;text-decoration:underline}
			.clear{clear:both}
			.left { float:left; display:inline-block}
			.wrap { width:960px; overflow:hidden; margin:0 auto;}
			#top { height:43px; overflow:hidden; background:#343434; width:100%}
			#top ul { margin-left:20px;}
			#top li { float:left; display:inline-block; padding:0 15px;}
			#top a { color:#fff; font-size:14px; line-height:43px;}
			.login { float:right; display:inline-block;}
			.login a { padding-left:10px;}
			.m404 {margin:100px auto; vertical-align:middle; text-align:center}
			</style>
			</head>
			<body>
			
		<div class="m404"><a target="blank" href="Main/main.do"><img src="images/404/404.png" width="409" height="177" alt="“真的很抱歉，我们搞丢了页面……”要不去网站首页看看？" border="0"></a></div>
</body>
</html>
